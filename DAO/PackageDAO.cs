﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;


namespace WebApplication1.DAO
{
    public class PackageDAO
    {

        public string cs = @"server=localhost;userid=root;
            password=Mihaica196;database=assign4";

        public MySqlConnection conn { get; set; }

        public PackageModel findPackage(int id)
        {
            MySqlDataReader rdr = null;
            PackageModel foundPackage = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM package WHERE packageId = @id";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                rdr = cmd.ExecuteReader();
                int firstPackage = 0;

                while (rdr.Read() && firstPackage == 0)
                {
                    string sender = getUserNameById(rdr.GetInt32(2));
                    string receiver = getUserNameById(rdr.GetInt32(3));
                    string senderCity = getCityNameById(rdr.GetInt32(4));
                    string receiverCity = getCityNameById(rdr.GetInt32(5));
                    firstPackage=1;
                    foundPackage = new PackageModel(rdr.GetInt32(0), rdr.GetString(1), rdr.GetString(6), sender, receiver,senderCity, receiverCity, rdr.GetBoolean(7));
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            return foundPackage;
        }

        private string getUserNameById(int id)
        {
            MySqlDataReader rdr = null;
            string userName = "";
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM user WHERE userId = @id";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                rdr = cmd.ExecuteReader();
                int firstUser = 0;


                while (rdr.Read() && firstUser == 0)
                {
                    firstUser = 1;
                    userName = rdr.GetString(1);
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            return userName;
        }

        private string getCityNameById(int id)
        {
            MySqlDataReader rdr = null;
            string cityName = "";
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM city WHERE cityId = @id";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                rdr = cmd.ExecuteReader();
                int firstCity = 0;
                

                while (rdr.Read() && firstCity == 0)
                {
                    firstCity = 1;
                    cityName = rdr.GetString(1);
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            return cityName;
        }

        public List<RouteModel> getAllRoutesForPackage(int packageId)
        {
            MySqlDataReader rdr = null;
            List<RouteModel> routes = new List<RouteModel>();

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM route WHERE packageId=@packageId";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@packageId", packageId);

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    string cityName=getCityNameById(rdr.GetInt32(2));
                    RouteModel foundRoute = new RouteModel(rdr.GetInt32(0), rdr.GetInt32(3), cityName, rdr.GetDateTime(1));
                    routes.Add(foundRoute);
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            return routes;
        }

        public List<PackageModel> getAllPackages(int userId)
        {
            MySqlDataReader rdr = null;
            List<PackageModel> packageModels = new List<PackageModel>();

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM package WHERE senderId = @userId OR receiverId = @userId";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@userId", userId);

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    string sender = getUserNameById(rdr.GetInt32(2));
                    string receiver = getUserNameById(rdr.GetInt32(3));
                    string senderCity = getCityNameById(rdr.GetInt32(4));
                    string receiverCity = getCityNameById(rdr.GetInt32(5));
                    PackageModel foundPackage = new PackageModel(rdr.GetInt32(0), rdr.GetString(1), rdr.GetString(6), sender, receiver, senderCity, receiverCity, rdr.GetBoolean(7));
                    packageModels.Add(foundPackage);
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            return packageModels;
        }
    }
}