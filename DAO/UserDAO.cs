﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.DAO
{
    public class UserDAO
    {

        public string cs = @"server=localhost;userid=root;
            password=Mihaica196;database=assign4";

        public MySqlConnection conn { get; set; }

        public void register(string username, string password)
        {
            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO user(username,password,role) VALUES(@username,@password,@role)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.Parameters.AddWithValue("@role", "client");
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        public UserModel login(string username)
        {
            MySqlDataReader rdr = null;
            UserModel foundUser = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SELECT * FROM user WHERE username = @username";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@username", username);

                rdr = cmd.ExecuteReader();
                int firstUser = 0;

                while (rdr.Read()&&firstUser==0)
                {
                    firstUser = 1;
                    Console.WriteLine(rdr.GetInt32(0) + ": "
                        + rdr.GetString(1) + rdr.GetString(2) + rdr.GetString(3));
                    foundUser = new UserModel(rdr.GetInt32(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3));
                }

                if (rdr != null)
                {
                    rdr.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            return foundUser;
        }
    }
}