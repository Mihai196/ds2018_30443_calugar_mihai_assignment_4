﻿using System;
using MySql.Data.MySqlClient;

public class Example
{

    static void Main()
    {
        string cs = @"server=localhost;userid=root;
            password=Mihaica196;database=assign4";

        MySqlConnection conn = null;

        try
        {
            conn = new MySqlConnection(cs);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "INSERT INTO city(Name,Latitude,Longitude) VALUES(@name,@latitude,@longtiude)";
            cmd.Prepare();

            cmd.Parameters.AddWithValue("@name", "Turda");
            cmd.Parameters.AddWithValue("@latitude", 15.2);
            cmd.Parameters.AddWithValue("@latitude", 17.3);
            cmd.ExecuteNonQuery();

        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Error: {0}", ex.ToString());

        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }

        }
    }
}