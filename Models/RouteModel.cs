﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class RouteModel
    {
        public int routeId { get; set; } 
        public int packageId { get; set; }
        public string cityName { get; set; }
        public DateTime time { get; set; } 

        public RouteModel (int routeId, int packageId,string cityName, DateTime time){
            this.routeId=routeId;
            this.packageId=packageId;
            this.cityName=cityName;
            this.time=time;
        }
    }
}