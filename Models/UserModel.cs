﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class UserModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string role { get; set; }

        public UserModel(int id, string username, string password, string role)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.role = role;
        }
    }
}