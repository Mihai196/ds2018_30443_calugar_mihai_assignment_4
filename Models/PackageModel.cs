﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class PackageModel
    {

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string senderName { get; set; }
        public string receiverName { get; set; }
        public string senderCityName { get; set; }
        public string destinationCityName { get; set; }
        public bool tracking { get; set; }

        public PackageModel(int id, string name, string description, string senderName, string receiverName, string senderCityName, string destinationCityName, bool tracking)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.senderName = senderName;
            this.receiverName = receiverName;
            this.senderCityName = senderCityName;
            this.destinationCityName = destinationCityName;
            this.tracking = tracking;
        }
    }
}