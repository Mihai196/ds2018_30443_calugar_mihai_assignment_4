
package webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="city" type="{http://service/}city" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="millisTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="aPackage" type="{http://service/}package" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeDTO", propOrder = {
    "city",
    "id",
    "millisTime",
    "aPackage"
})
public class RouteDTO {

    protected City city;
    protected int id;
    protected long millisTime;
    protected Package aPackage;

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity(City value) {
        this.city = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the millisTime property.
     * 
     */
    public long getMillisTime() {
        return millisTime;
    }

    /**
     * Sets the value of the millisTime property.
     * 
     */
    public void setMillisTime(long value) {
        this.millisTime = value;
    }

    /**
     * Gets the value of the aPackage property.
     * 
     * @return
     *     possible object is
     *     {@link Package }
     *     
     */
    public Package getAPackage() {
        return aPackage;
    }

    /**
     * Sets the value of the aPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Package }
     *     
     */
    public void setAPackage(Package value) {
        this.aPackage = value;
    }

}
