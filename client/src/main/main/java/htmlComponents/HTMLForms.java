package htmlComponents;

public class HTMLForms {
    public static String getClientForm(){
        return "<div class=\"form\">" +
                "<form action=\"/client\" method=\"POST\">\n" +
                "Search Package By Unique Id : <input type=\"text\" name=\"packageId\">\n" +
                "<br>\n" +
                "<input type=\"submit\" name=\"action\" value=\"ViewPackageDetails\">\n" +
                "<input type=\"submit\" name=\"action\" value=\"ViewPackageRoute\">\n" +
                "</form>"+
                "</div>";
    }

    public static String getAdminForm(){
        return "<div class=\"form\">" +
                "<form action=\"/admin\" method=\"POST\">\n" +
                "Search Package By Unique Id : <input type=\"text\" name=\"packageId\">\n" +
                "<br>\n" +
                "<input type=\"submit\" name=\"action\" value=\"AddPackage\">\n" +
                "<input type=\"submit\" name=\"action\" value=\"ViewPackageRoute\">\n" +
                "</form>"+
                "</div>";
    }
}
