package htmlComponents;

import org.hibernate.cfg.Configuration;
import webservices.AdminService;
import webservices.PackageDTO;
import webservices.PackageDTOArray;
import webservicesNET.ArrayOfPackageModel;
import webservicesNET.IClientService;
import webservicesNET.PackageModel;
import webservicesNET.RouteModel;

import java.util.List;

public class HTMLTables {

    public static String getTableFlights(IClientService clientService, int userId){

        ArrayOfPackageModel packages= clientService.getAllPackages(userId);
        List<PackageModel> packageModelList=packages.getPackageModel();
        String table = "<table border=\"1\" style=\"width:50%\">\n" +
                "  <tr>\n" +
                "    <th>Id</th>\n" +
                "    <th>Name</th>\n" +
                "    <th>Sender</th>\n" +
                "    <th>Receiver</th>\n" +
                "    <th>SenderCity</th>\n" +
                "    <th>DestinationCity</th>\n" +
                "    <th>Description</th>\n" +
                "    <th>Tracking</th>\n" +
                "  </tr>";
        for(PackageModel packageModel: packageModelList){

            table += "<tr>";
            table += "<td>" + packageModel.getX003CIdX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CNameX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CSenderNameX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CReceiverNameX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CSenderCityNameX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CDestinationCityNameX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.getX003CDescriptionX003EKBackingField() + "</td>";
            table += "<td>" + packageModel.isX003CTrackingX003EKBackingField() + "</td>";
            table += "</tr>";
        }
        return table;
    }

    public static String getTableRoutes(List<RouteModel> routeModels){
        String table = "<table border=\"1\" style=\"width:50%\">\n" +
                "  <tr>\n" +
                "    <th>Id</th>\n" +
                "    <th>CityName</th>\n" +
                "    <th>PackageId</th>\n" +
                "    <th>Date and time</th>\n" +
                "  </tr>";
        for(RouteModel routeModel:routeModels){
            table += "<tr>";
            table += "<td>" +routeModel.getX003CRouteIdX003EKBackingField() + "</td>";
            table += "<td>" + routeModel.getX003CCityNameX003EKBackingField()+ "</td>";
            table += "<td>" + routeModel.getX003CPackageIdX003EKBackingField() + "</td>";
            table += "<td>" + routeModel.getX003CTimeX003EKBackingField() + "</td>";
            table += "</tr>";
        }
        return table;
    }

    public static String getAllPackages(AdminService adminService){
        PackageDTOArray packageDTOArray=adminService.allPackages();
        List<PackageDTO> packageDTOS=packageDTOArray.getItem();
        String table = "<table border=\"1\" style=\"width:50%\">\n" +
                "  <tr>\n" +
                "    <th>Id</th>\n" +
                "    <th>Name</th>\n" +
                "    <th>Sender</th>\n" +
                "    <th>Receiver</th>\n" +
                "    <th>SenderCity</th>\n" +
                "    <th>DestinationCity</th>\n" +
                "    <th>Description</th>\n" +
                "    <th>Tracking</th>\n" +
                "  </tr>";

        for(PackageDTO packageDTO: packageDTOS){

            table += "<tr>";
            table += "<td>" + packageDTO.getId() + "</td>";
            table += "<td>" + packageDTO.getName() + "</td>";
            table += "<td>" + packageDTO.getSender().getUsername() + "</td>";
            table += "<td>" + packageDTO.getReceiver().getUsername() + "</td>";
            table += "<td>" + packageDTO.getSenderCity().getName() + "</td>";
            table += "<td>" + packageDTO.getDestinationCity().getName() + "</td>";
            table += "<td>" + packageDTO.getDescription() + "</td>";
            table += "<td>" + packageDTO.isTracking() + "</td>";
            table += "</tr>";
        }
        return table;
    }
}
