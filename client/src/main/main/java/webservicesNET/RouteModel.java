
package webservicesNET;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RouteModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="_x003C_cityName_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_packageId_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="_x003C_routeId_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="_x003C_time_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteModel", namespace = "http://schemas.datacontract.org/2004/07/WebApplication1.Models", propOrder = {
    "x003CCityNameX003EKBackingField",
    "x003CPackageIdX003EKBackingField",
    "x003CRouteIdX003EKBackingField",
    "x003CTimeX003EKBackingField"
})
public class RouteModel {

    @XmlElement(name = "_x003C_cityName_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CCityNameX003EKBackingField;
    @XmlElement(name = "_x003C_packageId_x003E_k__BackingField")
    protected int x003CPackageIdX003EKBackingField;
    @XmlElement(name = "_x003C_routeId_x003E_k__BackingField")
    protected int x003CRouteIdX003EKBackingField;
    @XmlElement(name = "_x003C_time_x003E_k__BackingField", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar x003CTimeX003EKBackingField;

    /**
     * Gets the value of the x003CCityNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CCityNameX003EKBackingField() {
        return x003CCityNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CCityNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CCityNameX003EKBackingField(String value) {
        this.x003CCityNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CPackageIdX003EKBackingField property.
     * 
     */
    public int getX003CPackageIdX003EKBackingField() {
        return x003CPackageIdX003EKBackingField;
    }

    /**
     * Sets the value of the x003CPackageIdX003EKBackingField property.
     * 
     */
    public void setX003CPackageIdX003EKBackingField(int value) {
        this.x003CPackageIdX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CRouteIdX003EKBackingField property.
     * 
     */
    public int getX003CRouteIdX003EKBackingField() {
        return x003CRouteIdX003EKBackingField;
    }

    /**
     * Sets the value of the x003CRouteIdX003EKBackingField property.
     * 
     */
    public void setX003CRouteIdX003EKBackingField(int value) {
        this.x003CRouteIdX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CTimeX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getX003CTimeX003EKBackingField() {
        return x003CTimeX003EKBackingField;
    }

    /**
     * Sets the value of the x003CTimeX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setX003CTimeX003EKBackingField(XMLGregorianCalendar value) {
        this.x003CTimeX003EKBackingField = value;
    }

}
