
package webservicesNET;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="findPackageResult" type="{http://schemas.datacontract.org/2004/07/WebApplication1.Models}PackageModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findPackageResult"
})
@XmlRootElement(name = "findPackageResponse")
public class FindPackageResponse {

    @XmlElementRef(name = "findPackageResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<PackageModel> findPackageResult;

    /**
     * Gets the value of the findPackageResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PackageModel }{@code >}
     *     
     */
    public JAXBElement<PackageModel> getFindPackageResult() {
        return findPackageResult;
    }

    /**
     * Sets the value of the findPackageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PackageModel }{@code >}
     *     
     */
    public void setFindPackageResult(JAXBElement<PackageModel> value) {
        this.findPackageResult = value;
    }

}
