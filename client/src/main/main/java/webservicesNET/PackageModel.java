
package webservicesNET;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PackageModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackageModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="_x003C_description_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_destinationCityName_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_id_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="_x003C_name_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_receiverName_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_senderCityName_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_senderName_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_tracking_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackageModel", namespace = "http://schemas.datacontract.org/2004/07/WebApplication1.Models", propOrder = {
    "x003CDescriptionX003EKBackingField",
    "x003CDestinationCityNameX003EKBackingField",
    "x003CIdX003EKBackingField",
    "x003CNameX003EKBackingField",
    "x003CReceiverNameX003EKBackingField",
    "x003CSenderCityNameX003EKBackingField",
    "x003CSenderNameX003EKBackingField",
    "x003CTrackingX003EKBackingField"
})
public class PackageModel {

    @XmlElement(name = "_x003C_description_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CDescriptionX003EKBackingField;
    @XmlElement(name = "_x003C_destinationCityName_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CDestinationCityNameX003EKBackingField;
    @XmlElement(name = "_x003C_id_x003E_k__BackingField")
    protected int x003CIdX003EKBackingField;
    @XmlElement(name = "_x003C_name_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CNameX003EKBackingField;
    @XmlElement(name = "_x003C_receiverName_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CReceiverNameX003EKBackingField;
    @XmlElement(name = "_x003C_senderCityName_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CSenderCityNameX003EKBackingField;
    @XmlElement(name = "_x003C_senderName_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CSenderNameX003EKBackingField;
    @XmlElement(name = "_x003C_tracking_x003E_k__BackingField")
    protected boolean x003CTrackingX003EKBackingField;

    /**
     * Gets the value of the x003CDescriptionX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CDescriptionX003EKBackingField() {
        return x003CDescriptionX003EKBackingField;
    }

    /**
     * Sets the value of the x003CDescriptionX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CDescriptionX003EKBackingField(String value) {
        this.x003CDescriptionX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CDestinationCityNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CDestinationCityNameX003EKBackingField() {
        return x003CDestinationCityNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CDestinationCityNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CDestinationCityNameX003EKBackingField(String value) {
        this.x003CDestinationCityNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CIdX003EKBackingField property.
     * 
     */
    public int getX003CIdX003EKBackingField() {
        return x003CIdX003EKBackingField;
    }

    /**
     * Sets the value of the x003CIdX003EKBackingField property.
     * 
     */
    public void setX003CIdX003EKBackingField(int value) {
        this.x003CIdX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CNameX003EKBackingField() {
        return x003CNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CNameX003EKBackingField(String value) {
        this.x003CNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CReceiverNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CReceiverNameX003EKBackingField() {
        return x003CReceiverNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CReceiverNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CReceiverNameX003EKBackingField(String value) {
        this.x003CReceiverNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CSenderCityNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CSenderCityNameX003EKBackingField() {
        return x003CSenderCityNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CSenderCityNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CSenderCityNameX003EKBackingField(String value) {
        this.x003CSenderCityNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CSenderNameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CSenderNameX003EKBackingField() {
        return x003CSenderNameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CSenderNameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CSenderNameX003EKBackingField(String value) {
        this.x003CSenderNameX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CTrackingX003EKBackingField property.
     * 
     */
    public boolean isX003CTrackingX003EKBackingField() {
        return x003CTrackingX003EKBackingField;
    }

    /**
     * Sets the value of the x003CTrackingX003EKBackingField property.
     * 
     */
    public void setX003CTrackingX003EKBackingField(boolean value) {
        this.x003CTrackingX003EKBackingField = value;
    }

}
