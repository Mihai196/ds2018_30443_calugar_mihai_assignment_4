
package webservicesNET;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPackageModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPackageModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PackageModel" type="{http://schemas.datacontract.org/2004/07/WebApplication1.Models}PackageModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPackageModel", namespace = "http://schemas.datacontract.org/2004/07/WebApplication1.Models", propOrder = {
    "packageModel"
})
public class ArrayOfPackageModel {

    @XmlElement(name = "PackageModel", nillable = true)
    protected List<PackageModel> packageModel;

    /**
     * Gets the value of the packageModel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packageModel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackageModel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackageModel }
     * 
     * 
     */
    public List<PackageModel> getPackageModel() {
        if (packageModel == null) {
            packageModel = new ArrayList<PackageModel>();
        }
        return this.packageModel;
    }

}
