
package webservicesNET;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRouteModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRouteModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteModel" type="{http://schemas.datacontract.org/2004/07/WebApplication1.Models}RouteModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRouteModel", namespace = "http://schemas.datacontract.org/2004/07/WebApplication1.Models", propOrder = {
    "routeModel"
})
public class ArrayOfRouteModel {

    @XmlElement(name = "RouteModel", nillable = true)
    protected List<RouteModel> routeModel;

    /**
     * Gets the value of the routeModel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeModel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteModel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteModel }
     * 
     * 
     */
    public List<RouteModel> getRouteModel() {
        if (routeModel == null) {
            routeModel = new ArrayList<RouteModel>();
        }
        return this.routeModel;
    }

}
