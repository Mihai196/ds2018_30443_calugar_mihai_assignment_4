
package webservicesNET;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="_x003C_id_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="_x003C_password_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_role_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="_x003C_username_x003E_k__BackingField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserModel", namespace = "http://schemas.datacontract.org/2004/07/WebApplication1.Models", propOrder = {
    "x003CIdX003EKBackingField",
    "x003CPasswordX003EKBackingField",
    "x003CRoleX003EKBackingField",
    "x003CUsernameX003EKBackingField"
})
public class UserModel {

    @XmlElement(name = "_x003C_id_x003E_k__BackingField")
    protected int x003CIdX003EKBackingField;
    @XmlElement(name = "_x003C_password_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CPasswordX003EKBackingField;
    @XmlElement(name = "_x003C_role_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CRoleX003EKBackingField;
    @XmlElement(name = "_x003C_username_x003E_k__BackingField", required = true, nillable = true)
    protected String x003CUsernameX003EKBackingField;

    /**
     * Gets the value of the x003CIdX003EKBackingField property.
     * 
     */
    public int getX003CIdX003EKBackingField() {
        return x003CIdX003EKBackingField;
    }

    /**
     * Sets the value of the x003CIdX003EKBackingField property.
     * 
     */
    public void setX003CIdX003EKBackingField(int value) {
        this.x003CIdX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CPasswordX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CPasswordX003EKBackingField() {
        return x003CPasswordX003EKBackingField;
    }

    /**
     * Sets the value of the x003CPasswordX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CPasswordX003EKBackingField(String value) {
        this.x003CPasswordX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CRoleX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CRoleX003EKBackingField() {
        return x003CRoleX003EKBackingField;
    }

    /**
     * Sets the value of the x003CRoleX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CRoleX003EKBackingField(String value) {
        this.x003CRoleX003EKBackingField = value;
    }

    /**
     * Gets the value of the x003CUsernameX003EKBackingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX003CUsernameX003EKBackingField() {
        return x003CUsernameX003EKBackingField;
    }

    /**
     * Sets the value of the x003CUsernameX003EKBackingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX003CUsernameX003EKBackingField(String value) {
        this.x003CUsernameX003EKBackingField = value;
    }

}
