package servlet;

import htmlComponents.HTMLForms;
import htmlComponents.HTMLTables;
import org.apache.commons.lang3.StringUtils;
import webservices.*;
import webservicesNET.ArrayOfRouteModel;
import webservicesNET.ClientService;
import webservicesNET.IClientService;
import webservicesNET.RouteModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class AdminServlet extends HttpServlet {

    private AdminService adminService;
    @Override
    public void init() throws ServletException {
        adminService=new AdminServiceImplService().getAdminServiceImplPort();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getSession().getAttribute("loggedUser")!=null&&
                request.getSession().getAttribute("loggedUserRole").equals("admin")) {

            List<RouteModel> routeModels = (List<RouteModel>) request.getSession().getAttribute("routesAdmin");
            String routeTable="";
            if(routeModels!=null){
                routeTable=HTMLTables.getTableRoutes(routeModels);
            }

            String clientPage =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                            "<html>" +
                            "<body>" +
                            "<br>" +
                            "<div class=\"form\">" +
                            "<form action=\"/admin\" method=\"POST\">\n" +
                            "Package name : <input type=\"text\" name=\"packageName\">\n" +
                            "Package description : <input type=\"text\" name=\"description\">\n" +
                            "Sender username : <input type=\"text\" name=\"sender\">\n" +
                            "Receiver username : <input type=\"text\" name=\"receiver\">\n" +
                            "<div>Sender City: "+
                            "<select name=\"senderCity\">\n" +
                            "  <option value=\"New York\">New York</option>\n" +
                            "  <option value=\"Bucharest\">Bucharest</option>\n" +
                            "  <option value=\"Teleorman\">Teleorman</option>\n" +
                            "  <option value=\"Turda\">Turda</option>\n" +
                            "</select>"+
                            "</div>"+
                            "<div>Destination City: "+
                            "<select name=\"destinationCity\">\n" +
                            "  <option value=\"New York\">New York</option>\n" +
                            "  <option value=\"Bucharest\">Bucharest</option>\n" +
                            "  <option value=\"Teleorman\">Teleorman</option>\n" +
                            "  <option value=\"Turda\">Turda</option>\n" +
                            "</select>"+
                            "</div>"+
                            "<br>\n" +
                            "<input type=\"submit\" name=\"action\" value=\"AddPackage\">\n" +
                            "<div>" +
                            "Package id for delete/tracking/routing : <input type=\"text\" name=\"packageId\">\n" +
                            "<input type=\"submit\" name=\"action\" value=\"DeletePackage\">\n" +
                            "<input type=\"submit\" name=\"action\" value=\"RegisterTracking\">\n" +

                            "<div>Route City: "+
                            "<select name=\"routeCity\">\n" +
                            "  <option value=\"New York\">New York</option>\n" +
                            "  <option value=\"Bucharest\">Bucharest</option>\n" +
                            "  <option value=\"Teleorman\">Teleorman</option>\n" +
                            "  <option value=\"Turda\">Turda</option>\n" +
                            "</select>"+
                            "</div>"+
                            "Timestamp route: <input type=\"datetime-local\" name=\"routeTime\"/>\n" +
                            "<input type=\"submit\" name=\"action\" value=\"AddRoute\">\n" +
                            "</div>" +
                            "</form>"+
                            "</div>" +
                            "<br>"+
                            "<div class=\"table\">" +
                            "       All packages:" +
                            HTMLTables.getAllPackages(adminService) +
                            routeTable +
                            "</div>" +
                            "<br>" +
                            "</body>" +
                            "</html>";
            response.setContentType("text/html");

            PrintWriter out = response.getWriter();
            out.println(clientPage);
        }
        else{
            response.sendRedirect("login");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action=request.getParameter("action");
        if(action!=null) {
            switch (action) {
                case "AddPackage":
                    addPackage(request, response);
                    break;
                case "DeletePackage":
                    removePackage(request,response);
                    break;
                case "RegisterTracking":
                    registerTracking(request,response);
                    break;
                case "AddRoute":
                    addRoute(request,response);
                    break;
                default:
                    break;
            }
        }
        else{
            doGet(request,response);
        }
    }

    public void addRoute(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String departureDate = request.getParameter("routeTime");
        if (StringUtils.countMatches(departureDate, ":") == 1) {
            departureDate += ":00";
        }
        Date departureDateDB= Timestamp.valueOf(departureDate.replace("T"," "));

        long millis=departureDateDB.getTime();
        String cityName=request.getParameter("routeCity");
        int packageId=Integer.parseInt(request.getParameter("packageId"));
        adminService.addRoute(packageId,cityName,millis);
        IClientService clientService=new ClientService().getBasicHttpBindingIClientService();
        ArrayOfRouteModel arrayOfRouteModel=clientService.getRoutes(packageId);
        List<RouteModel> routeModels=arrayOfRouteModel.getRouteModel();
        request.getSession().setAttribute("routesAdmin",routeModels);
        response.sendRedirect("/admin");
    }

    public void registerTracking(HttpServletRequest request, HttpServletResponse response) throws IOException {
        adminService.registerForTracking(Integer.parseInt(request.getParameter("packageId")));
        response.sendRedirect("/admin");
    }

    public void removePackage(HttpServletRequest request,HttpServletResponse response) throws IOException {
        adminService.deletePackage(Integer.parseInt(request.getParameter("packageId")));
        response.sendRedirect("/admin");
    }

    public void addPackage(HttpServletRequest request,HttpServletResponse response) throws IOException {
        PackageDTO packageDTO=new PackageDTO();
        City senderCity=new City();
        senderCity.setName(request.getParameter("senderCity"));
        City destinationCity=new City();
        destinationCity.setName(request.getParameter("destinationCity"));
        User sender=new User();
        sender.setUsername(request.getParameter("sender"));
        User receiver=new User();
        receiver.setUsername(request.getParameter("receiver"));
        packageDTO.setTracking(false);
        packageDTO.setName(request.getParameter("packageName"));
        packageDTO.setDescription(request.getParameter("description"));
        packageDTO.setSender(sender);
        packageDTO.setReceiver(receiver);
        packageDTO.setSenderCity(senderCity);
        packageDTO.setDestinationCity(destinationCity);
        adminService.addPackage(packageDTO);
        response.sendRedirect("/admin");
    }
}
