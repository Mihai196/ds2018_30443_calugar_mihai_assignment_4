package servlet;

import webservicesNET.ClientService;
import webservicesNET.IClientService;
import webservicesNET.UserModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {

    private IClientService clientService;

    @Override
    public void init() throws ServletException {
        clientService=new ClientService().getBasicHttpBindingIClientService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorMessage;
        if(request.getSession().getAttribute("ErrorMessage")==null){
            errorMessage="";
        }
        else{
            errorMessage=request.getSession().getAttribute("ErrorMessage").toString();
        }

        String page = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                "<html>\n" +
                "   <body>\n" +
                errorMessage+
                "      <form action = \"/login\" method = \"POST\">\n" +
                "         Username: <input type = \"text\" name = \"username\">\n" +
                "         <br />\n" +
                "         Password: <input type = \"password\" name = \"password\" />\n" +
                "         <br>" +
                "         <input type = \"submit\" name=\"action\" value = \"Login\" />\n" +
                "         <input type = \"submit\" name=\"action\" value = \"Register\" />\n" +
                "         <p  id=\"eroare\" name=\"eroare\"/>\n" +
                "      </form>\n" +
                "   </body>\n" +
                "</html>";
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println(page);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action=request.getParameter("action");
        if(action!=null) {
            switch (action) {
                case "Login":
                    loginUser(request, response);
                    break;
                case "Register":
                    registerRedirect(request, response);
                    break;
                default:
                    break;
            }
        }
        else{
            doGet(request,response);
        }
    }


    private void registerRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        clientService.register(username,password);
        UserModel user=clientService.login(username);
        request.getSession().setAttribute("loggedUser", username);
        request.getSession().setAttribute("userId",user.getX003CIdX003EKBackingField());
        request.getSession().setAttribute("loggedUserRole", "client");
        response.sendRedirect("/client");
    }


    private void loginUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        UserModel user=clientService.login(username);
        if(user!=null&&user.getX003CPasswordX003EKBackingField().equals(password)) {
            System.out.println(user.toString());
            if (user.getX003CRoleX003EKBackingField().equals("admin")) {
                request.getSession().setAttribute("loggedUser", username);
                request.getSession().setAttribute("loggedUserRole", "admin");
                response.sendRedirect("admin");
            } else {
                request.getSession().setAttribute("loggedUser", username);
                request.getSession().setAttribute("userId",user.getX003CIdX003EKBackingField());
                request.getSession().setAttribute("loggedUserRole", "client");
                response.sendRedirect("client");
            }
        }
        else{
            request.getSession().setAttribute("ErrorMessage","Invalid username or password. You can register by choosing username and password and click the register button");
            response.sendRedirect("login");
        }
    }
}
