package servlet;

import htmlComponents.HTMLForms;
import htmlComponents.HTMLTables;
import webservicesNET.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ClientServlet extends HttpServlet {


    private IClientService clientService;

    @Override
    public void init() throws ServletException {
        clientService=new ClientService().getBasicHttpBindingIClientService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getSession().getAttribute("loggedUser")!=null&&
                request.getSession().getAttribute("loggedUserRole").equals("client")) {
            List<RouteModel> routeModels = (List<RouteModel>) request.getSession().getAttribute("routes");
            String packageString = "";
            String routeTable="";
            if(routeModels!=null){
                routeTable=HTMLTables.getTableRoutes(routeModels);
            }
            if(request.getSession().getAttribute("packageString")!=null){
                packageString= (String) request.getSession().getAttribute("packageString");
            }

            String clientPage =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                            "<html>" +
                            "<body>" +
                            "<br>" +
                            packageString +
                            HTMLForms.getClientForm() +
                            routeTable +
                            "<br>"+
                            "<div class=\"table\">" +
                            "       Your packages:" +
                            HTMLTables.getTableFlights(clientService, (Integer) request.getSession().getAttribute("userId")) +
                            "<br>" +
                            "</body>" +
                            "</html>";
            response.setContentType("text/html");

            PrintWriter out = response.getWriter();
            out.println(clientPage);
        }
        else{
            response.sendRedirect("login");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action=request.getParameter("action");
        if(action!=null) {
            switch (action) {
                case "ViewPackageDetails":
                    viewPackageDetails(request, response);
                    break;
                case "ViewPackageRoute":
                    viewPackageRoute(request, response);
                    break;
                default:
                    break;
            }
        }
        else{
            doGet(request,response);
        }
    }

    public void viewPackageDetails(HttpServletRequest request,HttpServletResponse response) throws IOException {
        int packageId= Integer.parseInt(request.getParameter("packageId"));
        PackageModel packageModel=clientService.findPackage(packageId);
        System.out.println(packageModel.getX003CDescriptionX003EKBackingField());
        request.getSession().setAttribute("packageString",
                "Package Name : " + packageModel.getX003CNameX003EKBackingField()+
                        " || From : " + packageModel.getX003CSenderNameX003EKBackingField() +
                        " || To : " + packageModel.getX003CReceiverNameX003EKBackingField() +
                        " || Sender city : " + packageModel.getX003CSenderCityNameX003EKBackingField() +
                        " || Receiver city : " + packageModel.getX003CDestinationCityNameX003EKBackingField());
        response.sendRedirect("/client");
    }

    public void viewPackageRoute(HttpServletRequest request,HttpServletResponse response) throws IOException {
        int packageId= Integer.parseInt(request.getParameter("packageId"));
        ArrayOfRouteModel arrayOfRouteModel=clientService.getRoutes(packageId);
        List<RouteModel> routeModels=arrayOfRouteModel.getRouteModel();
        request.getSession().setAttribute("routes",routeModels);
        response.sendRedirect("/client");
    }
}
