package dto;

import entity.City;
import entity.Package;

import java.util.Date;

public class RouteDTO {

    private int id;

    public RouteDTO() {
    }

    public RouteDTO(int id, City city, long millisTime, Package aPackage) {
        this.id = id;
        this.city = city;
        this.millisTime = millisTime;
        this.aPackage = aPackage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    private City city;

    public long getMillisTime() {
        return millisTime;
    }

    public void setMillisTime(long millisTime) {
        this.millisTime = millisTime;
    }

    private long millisTime;
    private Package aPackage;
}
