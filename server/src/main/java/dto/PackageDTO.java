package dto;

import entity.City;
import entity.User;

public class PackageDTO {

    private int id;
    private String name;
    private String description;
    private User sender;
    private User receiver;
    private City senderCity;

    public PackageDTO(int id, String name, String description, User sender, User receiver, City senderCity, City destinationCity, boolean tracking) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.sender = sender;
        this.receiver = receiver;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public City getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(City senderCity) {
        this.senderCity = senderCity;
    }

    public City getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(City destinationCity) {
        this.destinationCity = destinationCity;
    }

    public PackageDTO(String name, String description, User sender, User receiver, City senderCity, City destinationCity, boolean tracking) {
        this.name = name;
        this.description = description;
        this.sender = sender;
        this.receiver = receiver;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    private City destinationCity;
    private boolean tracking;

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public PackageDTO(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
