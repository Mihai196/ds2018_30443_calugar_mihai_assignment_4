package entity;

public class Package {

    private int packageId;
    private String name;
    private String description;
    private User sender;
    private User receiver;
    private City senderCity;
    private City destinationCity;
    private boolean tracking;

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public Package(){
    }

    public Package(int packageId, String name, String description, User sender, User receiver, City senderCity, City destinationCity, boolean tracking) {
        this.packageId = packageId;
        this.name = name;
        this.description = description;
        this.sender = sender;
        this.receiver = receiver;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public Package(String name, String description, User sender, User receiver, City senderCity, City destinationCity, boolean tracking) {
        this.name = name;
        this.description = description;
        this.sender = sender;
        this.receiver = receiver;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public City getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(City senderCity) {
        this.senderCity = senderCity;
    }

    public City getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(City destinationCity) {
        this.destinationCity = destinationCity;
    }
}
