package entity;

import java.io.Serializable;
import java.util.Date;

public class Route implements Serializable {

    private int routeId;
    private Package packageR;
    private City city;
    private Date time;

    public Route(Package packageR, City city, Date time) {
        this.packageR = packageR;
        this.city = city;
        this.time = time;
    }

    public Route(int routeId, Package packageR, City city, Date time) {
        this.routeId = routeId;
        this.packageR = packageR;
        this.city = city;
        this.time = time;
    }

    public Route(){}
    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public Package getPackageR() {
        return packageR;
    }

    public void setPackageR(Package packageR) {
        this.packageR = packageR;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
