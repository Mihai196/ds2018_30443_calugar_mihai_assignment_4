package dao;

import entity.Package;
import org.hibernate.*;

import java.util.List;

public class PackageDAO {

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private SessionFactory sessionFactory;

    public PackageDAO (SessionFactory sessionFactory){
        this.sessionFactory=sessionFactory;
    }

    public int insertPackage(Package packageR) {

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int packageId=-1;
        try {
            tx = session.beginTransaction();
            packageId = (Integer) session.save(packageR);
            packageR.setPackageId(packageId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return packageId;
    }

    public void updatePackage(Package packageR) {

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(packageR);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    public void deletePackage(Package packageR)
    {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("DELETE FROM entity.Package WHERE id = :id");
            query.setParameter("id", packageR.getPackageId());
            query.executeUpdate();
            tx.commit();
        }
        finally {
            session.close();
        }
    }

    public Package findById(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packages = null;
        Package city = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM entity.Package WHERE packageId = :packageId");
            query.setParameter("packageId", id);
            packages = query.list();
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
        if(packages.size()==0){
            return null;
        }
        else{
            return packages.get(0);
        }
    }

    public List<Package> listPackages(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try {
            tx = session.beginTransaction();
            Query query=session.createQuery("FROM Package");
            packages=query.list();
            tx.commit();
            for(Package packageR:packages){
                Hibernate.initialize(packageR.getDestinationCity());
                Hibernate.initialize(packageR.getSenderCity());
                Hibernate.initialize(packageR.getSender());
                Hibernate.initialize(packageR.getReceiver());
            }
        }
        catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
        return packages;
    }
}
