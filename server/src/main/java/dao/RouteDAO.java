package dao;

import entity.Package;
import entity.Route;
import org.hibernate.*;

import java.util.List;

public class RouteDAO {

    public SessionFactory sessionFactory;

    public RouteDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int insertRoute(Route route) {

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int routeId=-1;
        try {
            tx = session.beginTransaction();
            routeId = (Integer) session.save(route);
            route.setRouteId(routeId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return routeId;
    }

    public List<Route> listRoutes(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            Query query=session.createQuery("FROM Route");
            routes=query.list();
            tx.commit();
            for(Route route:routes){
                Hibernate.initialize(route.getCity());
                Hibernate.initialize(route.getPackageR());
            }
        }
        catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
        return routes;
    }
}
