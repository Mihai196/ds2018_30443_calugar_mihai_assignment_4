package service;

import dto.PackageDTO;
import dto.RouteDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AdminService {

    @WebMethod int addPackage(PackageDTO packageDTO);
    @WebMethod void deletePackage(int id);
    @WebMethod void registerForTracking(int id);
    @WebMethod int addRoute(int packageId,String cityName, long timeInMillis);
    @WebMethod PackageDTO[] allPackages();
    @WebMethod RouteDTO[] allRoutes();


}
