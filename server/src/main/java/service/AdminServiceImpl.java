package service;

import dao.CityDAO;
import dao.PackageDAO;
import dao.RouteDAO;
import dao.UserDAO;
import dto.PackageDTO;
import dto.RouteDTO;
import entity.City;
import entity.Package;
import entity.Route;
import entity.User;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@WebService(endpointInterface = "service.AdminService")

public class AdminServiceImpl implements AdminService {
    public AdminServiceImpl() {
    }

    @Override
    public int addPackage(PackageDTO packageDTO) {
        PackageDAO packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
        CityDAO cityDAO=new CityDAO(new Configuration().configure().buildSessionFactory());
        UserDAO userDAO=new UserDAO(new Configuration().configure().buildSessionFactory());

        City senderCity=cityDAO.findByName(packageDTO.getSenderCity().getName());
        City destinationCity=cityDAO.findByName(packageDTO.getDestinationCity().getName());
        User senderUser=userDAO.findByUsername(packageDTO.getSender().getUsername());
        User receiverUser=userDAO.findByUsername(packageDTO.getReceiver().getUsername());
        Package packageR=new Package(packageDTO.getName(),packageDTO.getDescription(),senderUser,receiverUser,senderCity,destinationCity,packageDTO.isTracking());
        return packageDAO.insertPackage(packageR);
    }

    @Override
    public void deletePackage(int id) {
        PackageDAO packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
        Package packageR=packageDAO.findById(id);
        packageDAO.deletePackage(packageR);
    }

    @Override
    public void registerForTracking(int id) {
        PackageDAO packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
        CityDAO cityDAO=new CityDAO(new Configuration().configure().buildSessionFactory());
        RouteDAO routeDAO=new RouteDAO(new Configuration().configure().buildSessionFactory());
        Package packageR=packageDAO.findById(id);
        packageR.setTracking(true);
        packageDAO.updatePackage(packageR);
        Route routeSender=new Route(packageR,packageR.getSenderCity(),new Date(System.currentTimeMillis()));
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Route routeDestination= new Route(packageR,packageR.getDestinationCity(),new Date(System.currentTimeMillis() + (4 * DAY_IN_MS)));
        routeDAO.insertRoute(routeSender);
        routeDAO.insertRoute(routeDestination);
    }

    @Override
    public int addRoute(int packageId,String cityName, long timeInMillis) {
        PackageDAO packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
        CityDAO cityDAO=new CityDAO(new Configuration().configure().buildSessionFactory());
        RouteDAO routeDAO=new RouteDAO(new Configuration().configure().buildSessionFactory());
        City cityRoute=cityDAO.findByName(cityName);
        Package packageR=packageDAO.findById(packageId);
        Date date=new Date(timeInMillis);
        Route routePair=new Route(packageR,cityRoute,date);
        return routeDAO.insertRoute(routePair);
    }

    @Override
    public PackageDTO[] allPackages() {
        PackageDAO packageDAO = new PackageDAO(new Configuration().configure().buildSessionFactory());
        List<Package> packages=packageDAO.listPackages();
        List<PackageDTO> packageDTOS=new ArrayList<>();
        for(Package packageR:packages){
            PackageDTO packageDTO=
                    new PackageDTO(packageR.getPackageId(),packageR.getName(),packageR.getDescription(),
                            packageR.getSender(),packageR.getReceiver(),packageR.getSenderCity(),
                            packageR.getDestinationCity(),packageR.isTracking());
            packageDTOS.add(packageDTO);
        }
        PackageDTO[] packageDTOSarray=new PackageDTO[packageDTOS.size()];
        packageDTOSarray=packageDTOS.toArray(packageDTOSarray);
        return packageDTOSarray;
    }

    @Override
    public RouteDTO[] allRoutes() {
        RouteDAO routeDAO=new RouteDAO(new Configuration().configure().buildSessionFactory());

        List<Route> allroutes=routeDAO.listRoutes();
        List<RouteDTO> routeDTOS=new ArrayList<>();

        for(Route route:allroutes){
            RouteDTO routeDTO=new RouteDTO(route.getRouteId(),route.getCity(),route.getTime().getTime(),route.getPackageR());
            routeDTOS.add(routeDTO);
        }
        RouteDTO[] routes=new RouteDTO[routeDTOS.size()];
        routes=routeDTOS.toArray(routes);
        return routes;
    }
}
