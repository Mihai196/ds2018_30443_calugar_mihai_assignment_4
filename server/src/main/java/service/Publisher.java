package service;

import javax.xml.ws.Endpoint;
//Endpoint publisher

public class Publisher {
    public static void main(String[] args) {
        AdminServiceImpl adminService = new AdminServiceImpl();
        Endpoint.publish("http://localhost:7778/web/admin", adminService);
    }
}