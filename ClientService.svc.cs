﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebApplication1.DAO;
using WebApplication1.Models;

namespace WebApplication1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ClientService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ClientService.svc or ClientService.svc.cs at the Solution Explorer and start debugging.
    public class ClientService : IClientService
    {
        public void register(string username, string password)
        {
            UserDAO userDAO = new UserDAO();
            userDAO.register(username, password);
        }

        public UserModel login(string username)
        {
            UserDAO userDAO = new UserDAO();
            return userDAO.login(username);
        }

        public List<PackageModel> getAllPackages(int userId)
        {
            PackageDAO packageDAO = new PackageDAO();
            return packageDAO.getAllPackages(userId);
        }

        public PackageModel findPackage(int id)
        {
            PackageDAO packageDAO = new PackageDAO();
            return packageDAO.findPackage(id);
        }

        public List<RouteModel> getRoutes(int packageId)
        {
            PackageDAO packageDAO = new PackageDAO();
            return packageDAO.getAllRoutesForPackage(packageId);
        }
    }
}

